#!/usr/bin/env python

# Description: Enforces a list of protected branches over projects.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-de/use-cases/
GROUP_ID = os.environ.get('GL_GROUP_ID', 16058698)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

PROTECTED_BRANCHES = {
    'main': {
        'merge_access_level': gitlab.const.AccessLevel.MAINTAINER,
        'push_access_level': gitlab.const.AccessLevel.MAINTAINER
    },
    'production': {
        'merge_access_level': gitlab.const.AccessLevel.MAINTAINER,
        'push_access_level': gitlab.const.AccessLevel.MAINTAINER
    },
}

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
group = gl.groups.get(GROUP_ID)

# Collect all projects in group and subgroups
projects = group.projects.list(include_subgroups=True, all=True)

for project in projects:
    # Retrieve a full manageable project object
    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
    manageable_project = gl.projects.get(project.id)

    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/protected_branches.html
    protected_branch_names = []

    for pb in manageable_project.protectedbranches.list():
        manageable_protected_branch = manageable_project.protectedbranches.get(pb.name)
        print("Protected branch name: {n}, merge_access_level: {mal}, push_access_level: {pal}".format(
            n=manageable_protected_branch.name,
            mal=manageable_protected_branch.merge_access_levels,
            pal=manageable_protected_branch.push_access_levels
        ))

        protected_branch_names.append(manageable_protected_branch.name)

    for branch_to_protect, levels in PROTECTED_BRANCHES.items():
        # Fix missing protected branches
        if branch_to_protect not in protected_branch_names:
            print("Adding branch {n} to protected branches settings".format(n=branch_to_protect))
            p_branch = manageable_project.protectedbranches.create({
                'name': branch_to_protect,
                'merge_access_level': gitlab.const.AccessLevel.MAINTAINER,
                'push_access_level': gitlab.const.AccessLevel.MAINTAINER
            })



