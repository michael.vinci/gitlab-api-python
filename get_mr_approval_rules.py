#!/usr/bin/env python

# Description: Fetch projects (all, group, project) and print Merge request approval settings.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys
import json

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires maintainer+ permissions
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
GROUP_ID = os.environ.get('GL_GROUP_ID') #optional

#################
# Main

if __name__ == "__main__":
    if not GITLAB_TOKEN:
        print("🤔 Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

    # Collect all projects, or prefer projects from a group id, or a project id
    projects = []

    # Direct project ID
    if PROJECT_ID:
        projects.append(gl.projects.get(PROJECT_ID))

    # Groups and projects inside
    elif GROUP_ID:
        group = gl.groups.get(GROUP_ID)

        for project in group.projects.list(include_subgroups=True, all=True):
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            manageable_project = gl.projects.get(project.id)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        projects = gl.projects.list(get_all=True)

    print("# Summary of projects and their merge request approval rules")

    # Loop over projects and print the settings
    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_request_approvals.html
    for project in projects:
        if len(project.approvalrules.list()) > 0:
            #print(project) #debug
            print("# Project: {name}, ID: {id}\n\n".format(name=project.name_with_namespace, id=project.id))
            print("[MR Approval settings]({url}/-/settings/merge_requests)\n\n".format(url=project.web_url))

            for ar in project.approvalrules.list():
                print("## Approval rule: {name}, ID: {id}".format(name=ar.name, id=ar.id))
                print("\n```json\n")
                print(json.dumps(ar.attributes, indent=2)) # TODO: can be more beautiful, but serves its purpose with pretty print JSON
                print("\n```\n")

sys.exit(0)